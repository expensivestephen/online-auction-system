Given(/^the following auctions are currently available for bidding$/) do |table|
  # table is a Cucumber::Core::Ast::DataTable
  pending # Write code here that turns the phrase above into concrete actions
end

Given(/^I am in the "([^"]*)" web page$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end

When(/^I query the auction page for "([^"]*)"$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end

When(/^I bid (\d+)\.(\d+) for the queried item$/) do |arg1, arg2|
  pending # Write code here that turns the phrase above into concrete actions
end

Then(/^the price on the queried item should be updated to (\d+)\.(\d+)$/) do |arg1, arg2|
  pending # Write code here that turns the phrase above into concrete actions
end

Then(/^the bid is "([^"]*)"$/) do |arg1|
  pending # Write code here that turns the phrase above into concrete actions
end