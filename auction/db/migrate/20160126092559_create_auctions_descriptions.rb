class CreateAuctionsDescriptions < ActiveRecord::Migration
  def change
    create_table :auctions_descriptions do |t|
      t.string :description
      t.float :currentprice
      t.date :end_date

      t.timestamps null: false
    end
  end
end
