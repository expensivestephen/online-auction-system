class CreateOpenAuctions < ActiveRecord::Migration
  def change
    create_table :open_auctions do |t|
      t.string :description
      t.float :price
      t.date :end_date

      t.timestamps null: false
    end
  end
end
