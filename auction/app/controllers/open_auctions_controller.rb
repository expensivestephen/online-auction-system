class OpenAuctionsController < ApplicationController
  before_action :set_open_auction, only: [:show, :edit, :update, :destroy]

  # GET /open_auctions
  # GET /open_auctions.json
  def index
    @open_auctions = OpenAuction.all
  end

  # GET /open_auctions/1
  # GET /open_auctions/1.json
  def show
  end

  # GET /open_auctions/new
  def new
    @open_auction = OpenAuction.new
  end

  # GET /open_auctions/1/edit
  def edit
  end

  # POST /open_auctions
  # POST /open_auctions.json
  def create
    @open_auction = OpenAuction.new(open_auction_params)

    respond_to do |format|
      if @open_auction.save
        format.html { redirect_to @open_auction, notice: 'Open auction was successfully created.' }
        format.json { render :show, status: :created, location: @open_auction }
      else
        format.html { render :new }
        format.json { render json: @open_auction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /open_auctions/1
  # PATCH/PUT /open_auctions/1.json
  def update
    respond_to do |format|
      if @open_auction.update(open_auction_params)
        format.html { redirect_to @open_auction, notice: 'Open auction was successfully updated.' }
        format.json { render :show, status: :ok, location: @open_auction }
      else
        format.html { render :edit }
        format.json { render json: @open_auction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /open_auctions/1
  # DELETE /open_auctions/1.json
  def destroy
    @open_auction.destroy
    respond_to do |format|
      format.html { redirect_to open_auctions_url, notice: 'Open auction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_open_auction
      @open_auction = OpenAuction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def open_auction_params
      params.require(:open_auction).permit(:description, :price, :end_date)
    end
end
