class AuctionsDescriptionsController < ApplicationController
  before_action :set_auctions_description, only: [:show, :edit, :update, :destroy]

  # GET /auctions_descriptions
  # GET /auctions_descriptions.json
  def index
    @auctions_descriptions = AuctionsDescription.all
  end

  # GET /auctions_descriptions/1
  # GET /auctions_descriptions/1.json
  def show
  end

  # GET /auctions_descriptions/new
  def new
    @auctions_description = AuctionsDescription.new
  end

  # GET /auctions_descriptions/1/edit
  def edit
  end

  # POST /auctions_descriptions
  # POST /auctions_descriptions.json
  def create
    @auctions_description = AuctionsDescription.new(auctions_description_params)

    respond_to do |format|
      if @auctions_description.save
        format.html { redirect_to @auctions_description, notice: 'Auctions description was successfully created.' }
        format.json { render :show, status: :created, location: @auctions_description }
      else
        format.html { render :new }
        format.json { render json: @auctions_description.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /auctions_descriptions/1
  # PATCH/PUT /auctions_descriptions/1.json
  def update
    respond_to do |format|
      if @auctions_description.update(auctions_description_params)
        format.html { redirect_to @auctions_description, notice: 'Auctions description was successfully updated.' }
        format.json { render :show, status: :ok, location: @auctions_description }
      else
        format.html { render :edit }
        format.json { render json: @auctions_description.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /auctions_descriptions/1
  # DELETE /auctions_descriptions/1.json
  def destroy
    @auctions_description.destroy
    respond_to do |format|
      format.html { redirect_to auctions_descriptions_url, notice: 'Auctions description was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_auctions_description
      @auctions_description = AuctionsDescription.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def auctions_description_params
      params.require(:auctions_description).permit(:description, :currentprice, :end_date)
    end
end
