json.array!(@open_auctions) do |open_auction|
  json.extract! open_auction, :id, :description, :price, :end_date
  json.url open_auction_url(open_auction, format: :json)
end
