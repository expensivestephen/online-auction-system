json.array!(@auctions_descriptions) do |auctions_description|
  json.extract! auctions_description, :id, :description, :currentprice, :end_date
  json.url auctions_description_url(auctions_description, format: :json)
end
