Feature: Querying available auctions and Placing a bid
  As a customer
  So that I can place a bid on an auction
  I want to see all open auctions

  Background: Open auction
	  Given the following auctions are currently available for bidding
		  | description					                    | price	| closing date	|
		  | Roller Derby Brand Blade Skate (Size 7)	| 29.00	| 29/01/2016	  |
		  | Chicago Bullet Speed Skate (Size 7)		  | 59.00	| 26/10.2016	  |
		  | Riedell Dart Derby Skates (Size 8)		  | 106.00| 30/01/2016	  |
	  And I am in the "open auction" web page

  Scenario: Offer 62.00 for chicago bullet speed skate (size 7)
    When I query the open auction page for "Chicago Bullet Speed Skate (Size 7)"
     And I offer 62.00 in the auction details
    Then the price on the open auction should be updated to 62.00

  Scenario: Offer 50.00 for ridell dart derby skate (size 8)
    When I query the open auction page for "Ridell Dart Derby Skate (Size 8)"
     And I offer 50.00 as my bid
    Then the bid is "rejected"