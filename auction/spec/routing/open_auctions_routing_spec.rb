require "rails_helper"

RSpec.describe OpenAuctionsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/open_auctions").to route_to("open_auctions#index")
    end

    it "routes to #new" do
      expect(:get => "/open_auctions/new").to route_to("open_auctions#new")
    end

    it "routes to #show" do
      expect(:get => "/open_auctions/1").to route_to("open_auctions#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/open_auctions/1/edit").to route_to("open_auctions#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/open_auctions").to route_to("open_auctions#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/open_auctions/1").to route_to("open_auctions#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/open_auctions/1").to route_to("open_auctions#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/open_auctions/1").to route_to("open_auctions#destroy", :id => "1")
    end

  end
end
