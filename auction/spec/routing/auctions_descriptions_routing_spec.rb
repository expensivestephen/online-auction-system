require "rails_helper"

RSpec.describe AuctionsDescriptionsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/auctions_descriptions").to route_to("auctions_descriptions#index")
    end

    it "routes to #new" do
      expect(:get => "/auctions_descriptions/new").to route_to("auctions_descriptions#new")
    end

    it "routes to #show" do
      expect(:get => "/auctions_descriptions/1").to route_to("auctions_descriptions#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/auctions_descriptions/1/edit").to route_to("auctions_descriptions#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/auctions_descriptions").to route_to("auctions_descriptions#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/auctions_descriptions/1").to route_to("auctions_descriptions#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/auctions_descriptions/1").to route_to("auctions_descriptions#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/auctions_descriptions/1").to route_to("auctions_descriptions#destroy", :id => "1")
    end

  end
end
