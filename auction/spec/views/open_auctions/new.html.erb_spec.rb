require 'rails_helper'

RSpec.describe "open_auctions/new", type: :view do
  before(:each) do
    assign(:open_auction, OpenAuction.new(
      :description => "MyString",
      :price => 1.5
    ))
  end

  it "renders new open_auction form" do
    render

    assert_select "form[action=?][method=?]", open_auctions_path, "post" do

      assert_select "input#open_auction_description[name=?]", "open_auction[description]"

      assert_select "input#open_auction_price[name=?]", "open_auction[price]"
    end
  end
end
