require 'rails_helper'

RSpec.describe "open_auctions/show", type: :view do
  before(:each) do
    @open_auction = assign(:open_auction, OpenAuction.create!(
      :description => "Description",
      :price => 1.5
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Description/)
    expect(rendered).to match(/1.5/)
  end
end
