require 'rails_helper'

RSpec.describe "open_auctions/index", type: :view do
  before(:each) do
    assign(:open_auctions, [
      OpenAuction.create!(
        :description => "Description",
        :price => 1.5
      ),
      OpenAuction.create!(
        :description => "Description",
        :price => 1.5
      )
    ])
  end

  it "renders a list of open_auctions" do
    render
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
