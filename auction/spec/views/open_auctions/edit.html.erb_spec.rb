require 'rails_helper'

RSpec.describe "open_auctions/edit", type: :view do
  before(:each) do
    @open_auction = assign(:open_auction, OpenAuction.create!(
      :description => "MyString",
      :price => 1.5
    ))
  end

  it "renders the edit open_auction form" do
    render

    assert_select "form[action=?][method=?]", open_auction_path(@open_auction), "post" do

      assert_select "input#open_auction_description[name=?]", "open_auction[description]"

      assert_select "input#open_auction_price[name=?]", "open_auction[price]"
    end
  end
end
