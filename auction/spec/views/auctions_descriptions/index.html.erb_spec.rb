require 'rails_helper'

RSpec.describe "auctions_descriptions/index", type: :view do
  before(:each) do
    assign(:auctions_descriptions, [
      AuctionsDescription.create!(
        :description => "Description",
        :currentprice => 1.5
      ),
      AuctionsDescription.create!(
        :description => "Description",
        :currentprice => 1.5
      )
    ])
  end

  it "renders a list of auctions_descriptions" do
    render
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
