require 'rails_helper'

RSpec.describe "auctions_descriptions/edit", type: :view do
  before(:each) do
    @auctions_description = assign(:auctions_description, AuctionsDescription.create!(
      :description => "MyString",
      :currentprice => 1.5
    ))
  end

  it "renders the edit auctions_description form" do
    render

    assert_select "form[action=?][method=?]", auctions_description_path(@auctions_description), "post" do

      assert_select "input#auctions_description_description[name=?]", "auctions_description[description]"

      assert_select "input#auctions_description_currentprice[name=?]", "auctions_description[currentprice]"
    end
  end
end
