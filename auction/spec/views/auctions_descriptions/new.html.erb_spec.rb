require 'rails_helper'

RSpec.describe "auctions_descriptions/new", type: :view do
  before(:each) do
    assign(:auctions_description, AuctionsDescription.new(
      :description => "MyString",
      :currentprice => 1.5
    ))
  end

  it "renders new auctions_description form" do
    render

    assert_select "form[action=?][method=?]", auctions_descriptions_path, "post" do

      assert_select "input#auctions_description_description[name=?]", "auctions_description[description]"

      assert_select "input#auctions_description_currentprice[name=?]", "auctions_description[currentprice]"
    end
  end
end
