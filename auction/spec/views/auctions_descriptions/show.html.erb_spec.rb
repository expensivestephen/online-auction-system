require 'rails_helper'

RSpec.describe "auctions_descriptions/show", type: :view do
  before(:each) do
    @auctions_description = assign(:auctions_description, AuctionsDescription.create!(
      :description => "Description",
      :currentprice => 1.5
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Description/)
    expect(rendered).to match(/1.5/)
  end
end
