require 'rails_helper'

RSpec.describe "bids/index", type: :view do
  before(:each) do
    assign(:bids, [
      Bid.create!(
        :amount => 1.5
      ),
      Bid.create!(
        :amount => 1.5
      )
    ])
  end

  it "renders a list of bids" do
    render
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
